package pl.globallogic.Interfaces;

public interface Drivable
{
    void start();

    void stop ();

    void ignite();

    default void method1()
    {

    }

    default void switchLights()
    {
        System.out.println("Switch");
    }
}
