package pl.globallogic.exercises.intermediate;

import java.util.Arrays;

public class Ex43ReverseArray
{
    /*

    Write a method called reverse() with an int array as a parameter.
    The method should not return any value. In other words, the method is allowed to modify the array parameter.
    To reverse the array, you have to swap the elements, so that the first element is swapped with the last element and so on.

     */

    public static void main(String[] args)
    {
        int[] array = {0,3,67,2,8,7};

        reverse(array);
    }

    public static void reverse(int[] array)
    {
        int temp = 0;

        System.out.println(Arrays.toString(array));


        for (int i = 0; i < array.length / 2 ; i++)
        {
            temp = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = temp;
        }

        System.out.println(Arrays.toString(array));
    }
}
