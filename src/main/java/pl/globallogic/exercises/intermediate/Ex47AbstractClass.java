package pl.globallogic.exercises.intermediate;

public class Ex47AbstractClass
{
    /*

    Create an abstract class to define items that can be stored in a tree.
    Implement the following:
    1. ListItem (abstract class)
    2. Node (concrete class)
    3. MyLindedList (concrete class)
    4. NodeList (interface)
    5. SearchTree (concrete class)

     */

    public static void main(String[] args)
    {
        Node main = new Node(5);
        MyLinkedList list = new MyLinkedList(main);
        list.traverse(null);

        Node node2 = new Node(7);
        main.setNext(node2);

        Node node3 = new Node(2);
        node3.setPrevious(node2);

        node2.setNext(node3);
        node2.setPrevious(main);

        System.out.println("----\n");
        list.traverse(main);

        list.removeItem(node2);
        System.out.println("----\n");
        list.traverse(main);
    }

    static abstract class ListItem
    {
        protected ListItem rightLink = null, leftLink = null;
        protected Object value;

        public ListItem(Object value)
        {
            this.value = value;
        }

        abstract ListItem next();
        abstract ListItem setNext(ListItem object);
        abstract ListItem previous();
        abstract ListItem setPrevious(ListItem object);
        abstract int compareTo(ListItem object);

        public Object getValue()
        {
            return value;
        }

        public void setValue(Object value)
        {
            this.value = value;
        }
    }

    static class Node extends ListItem
    {
        public Node(Object value)
        {
            super(value);
        }

        @Override
        ListItem next()
        {
            return this.rightLink;
        }
        @Override
        ListItem setNext(ListItem object)
        {
            this.rightLink = object;
            return this.rightLink;
        }
        @Override
        ListItem previous()
        {
            return this.leftLink;
        }
        @Override
        ListItem setPrevious(ListItem object)
        {
            this.leftLink = object;
            return this.leftLink;
        }
        @Override
        int compareTo(ListItem valueCompare)
        {
            if(valueCompare != null)
                return (String.valueOf(super.getValue()).compareTo(String.valueOf(valueCompare.getValue())));
            else
                return -1;
        }
    }

    static class MyLinkedList implements NodeList
    {
        private ListItem root = null;

        public MyLinkedList(ListItem root)
        {
            this.root = root;
        }

        @Override
        public ListItem getRoot()
        {
            return root;
        }

        @Override
        public boolean addItem(ListItem object)
        {
            if(this.root == null)
            {
                this.root = object;
                return true;
            }

            ListItem current = this.root;

            while (object != null)
            {
                if (current.compareTo(object) < 0)
                {
                    if(current.next() != null)
                    {
                        current = current.next();
                    }
                    else
                    {
                        current.setNext(object);
                        object.setPrevious(current);
                        return true;
                    }
                }
            }

            return false;
        }

        @Override
        public boolean removeItem(ListItem object)
        {
            if (this.root.getValue() == object)
            {
                if(this.root.next() != null)
                {
                    this.root.next().leftLink = null;
                    this.root = this.root.next();
                }
                else
                {
                    this.root = null;
                }
            }

            ListItem current = this.root;
            while (current != null)
            {
                if (current.compareTo(object) < 0)
                {
                    if (current.next() != null)
                    {
                        current = current.next();
                    }
                    else
                    {
                        try
                        {
                            current.previous().rightLink = current.next();
                        }
                        catch(NullPointerException e)
                        {
                            break;
                        }

                        try
                        {
                            current.next().leftLink = current.previous();
                        }
                        catch(NullPointerException e)
                        {
                            break;
                        }

                        current = null;

                        return true;
                    }
                }
                current = current.next();
            }

            return false;
        }

        @Override
        public void traverse(ListItem root)
        {
            if(root == null)
                System.out.println("List is empty\n");

            ListItem curr = root;

            while (curr != null)
            {
                System.out.printf("%s\n", (Object) curr.value);
                curr = curr.next();
            }
        }
    }

    static interface NodeList
    {
        abstract ListItem getRoot();
        abstract boolean addItem(ListItem object);
        abstract boolean removeItem(ListItem object);
        abstract void traverse(ListItem root);
    }

    static class SearchTree implements NodeList
    {
        ListItem root = null;

        public SearchTree(ListItem root)
        {
            this.root = root;
        }

        @Override
        public ListItem getRoot()
        {
            return root;
        }

        @Override
        public boolean addItem(ListItem object)
        {
            if(this.root == null)
            {
                this.root = object;
                return true;
            }

            ListItem current = this.root;

            while (object != null)
            {
                if (current.compareTo(object) < 0)
                {
                    if(current.next() != null)
                    {
                        current = current.next();
                    }
                    else
                    {
                        current.setNext(object);
                        object.setPrevious(current);
                        return true;
                    }
                }
            }

            return false;
        }

        @Override
        public boolean removeItem(ListItem object)
        {
            if (this.root.getValue() == object)
            {
                if(this.root.next() != null)
                {
                    this.root.next().leftLink = null;
                    this.root = this.root.next();
                }
                else
                {
                    this.root = null;
                }
            }

            ListItem current = this.root;
            while (current != null)
            {
                if (current.compareTo(object) < 0)
                {
                    if (current.next() != null)
                    {
                        current = current.next();
                    }
                    else
                    {
                        current.previous().rightLink = current.next();
                        current.next().leftLink = current.previous();
                        current = null;

                        return true;
                    }
                }
            }

            return false;
        }

        private void preformRemoval(ListItem parent, ListItem objectToRemove)
        {

        }

        @Override
        public void traverse(ListItem root)
        {
            if(root == null)
                return;

            traverse(root.leftLink);

            System.out.printf("%s ", (String)root.value);

            traverse(root.rightLink);
        }
    }
}
