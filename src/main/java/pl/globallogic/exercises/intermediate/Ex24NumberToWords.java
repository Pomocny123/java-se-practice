package pl.globallogic.exercises.intermediate;

public class Ex24NumberToWords
{
    /*

    The method should print out the passed number using words
    for the digits.

     */
    public static void main(String[] args)
    {
        numberToWords(123);
        numberToWords(1010);
        numberToWords(1000);
        numberToWords(-12);
    }

    private static void numberToWords(int number)
    {
        if(number < 0)
        {
            System.out.println("Invalid value");
            return;
        }

        int lengthOfNumber = 0;

        lengthOfNumber = getDigitCount(number);
        number = reverse(number);

        int copyNumber = number;

        copyNumber = number;
        int[] singleDigits = new int[lengthOfNumber];
        int iteration = 0;

        while (copyNumber > 0)
        {
            singleDigits[iteration] = copyNumber % 10;

            copyNumber /= 10;
            iteration++;
        }

        String numberAsWords = new String();

        for (int i = 0; i < lengthOfNumber; i++)
        {
            switch (singleDigits[i])
            {
                case 0:
                        numberAsWords += "ZERO ";
                    break;
                    case 1:
                        numberAsWords += "ONE ";
                    break;
                    case 2:
                        numberAsWords += "TWO ";
                    break;
                    case 3:
                        numberAsWords += "THREE ";
                    break;
                    case 4:
                        numberAsWords += "FOUR ";
                    break;
                    case 5:
                        numberAsWords += "FIVE ";
                    break;
                    case 6:
                        numberAsWords += "SIX ";
                    break;
                    case 7:
                        numberAsWords += "SEVEN ";
                    break;
                    case 8:
                        numberAsWords += "EIGTH ";
                    break;
                    case 9:
                        numberAsWords += "NINE ";
                    break;
            }
        }
        System.out.println(numberAsWords);
    }

    private static int reverse(int number)
    {
        int reversedNumber = 0;

        while (number > 0)
        {
            reversedNumber = (reversedNumber * 10) + (number % 10);

            number /= 10;
        }

        return reversedNumber;
    }

    private static int getDigitCount(int number)
    {
        int lengthOfNumber = 0;

        if(number == 0) lengthOfNumber = 1;

        while (number > 0)
        {
            lengthOfNumber++;
            number /= 10;
        }

        return lengthOfNumber;
    }
}
