package pl.globallogic.exercises.intermediate;

public class Ex20LastDigitChecker
{
    /*

    Write a method named hasSameLastDigit with three parameters of type int.
    Each number should be within the range of 10 (inclusive) - 1000 (inclusive).

    If one of the numbers is not within the range, the method should return false.

    The method should return true if at least two of the numbers share the same
    rightmost digit; otherwise, it should return false.

     */
    public static void main(String[] args)
    {
        System.out.println(hasSameLastDigit(41,22,71));
        System.out.println(hasSameLastDigit(23,32,42));
        System.out.println(hasSameLastDigit(9,99,999));
        System.out.println(hasSameLastDigit(12,33,44));
    }

    private static boolean hasSameLastDigit(int firstNumber, int secondNumber, int thirdNumber)
    {
        if (firstNumber < 10 || firstNumber > 1000 ||
                secondNumber < 10 || secondNumber > 1000 ||
                thirdNumber < 10 || thirdNumber > 1000)
            return false;

        int[] lastDigitOfNumbers = {(firstNumber % 10),
                (secondNumber % 10),
                (thirdNumber % 10)};

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                if(i != j)
                    if(lastDigitOfNumbers[i] == lastDigitOfNumbers[j])
                        return true;
            }
        }

        return false;
    }
}
