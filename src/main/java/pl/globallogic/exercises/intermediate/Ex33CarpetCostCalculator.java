package pl.globallogic.exercises.intermediate;

public class Ex33CarpetCostCalculator
{
    /*

    The Carpet Company has asked you to write an application that
    calculates the price of carpeting for rectangular rooms. To
    calculate the price, you multiply the area of the floor (width
    times length) by the price per square meter of carpet. For
    example, the area of the floor that is 12 meters long and 10
    meters wide is 120 square meters. To cover the floor with a
    carpet that costs $8 per square meter would cost $960.

     */
    public static void main(String[] args)
    {
        Carpet carpet = new Carpet(3.5);
        Floor floor = new Floor(2.75, 4.0);
        Calculator calculator = new Calculator(floor, carpet);
        System.out.println("total= " + calculator.getTotalCost());
        carpet = new Carpet(1.5);
        floor = new Floor(5.4, 4.5);
        calculator = new Calculator(floor, carpet);
        System.out.println("total= " + calculator.getTotalCost());
    }

    static class Floor
    {
        double width, length;

        public Floor(double width, double length)
        {
            if(width >= 0)
                this.width = width;
            else
                this.width = 0;

            if(length >= 0)
                this.length = length;
            else
                this.length = 0;
        }

        private double getArea()
        {
            return this.width * this.length;
        }
    }

    static class Carpet
    {
        double cost;

        public Carpet(double cost)
        {
            if(cost >= 0)
                this.cost = cost;
            else
                this.cost = 0;
        }

        public double getCost()
        {
            return cost;
        }
    }

    static class Calculator
    {
        Floor floor;
        Carpet carpet;

        public Calculator(Floor floor, Carpet carpet)
        {
            this.floor = floor;
            this.carpet = carpet;
        }

        private double  getTotalCost()
        {
            return floor.getArea() * carpet.getCost();
        }
    }
}
