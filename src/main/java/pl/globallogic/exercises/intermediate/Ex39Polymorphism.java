package pl.globallogic.exercises.intermediate;

public class Ex39Polymorphism
{
    /*

    For this exercise you will create four classes of vehicles.
    The first class should be named Car. This will be the base
    class for three other classes, Mitsubishi, Holden, and Ford.



     */
    public static void main(String[] args)
    {
        Car car = new Car(8, "Base car");
        car.startEngine();
        car.accelerate();
        car.brake();

        Mitsubishi mitsubishi = new Mitsubishi(6, "Outlander VRX 4WD");
        mitsubishi.startEngine();
        mitsubishi.accelerate();
        mitsubishi.brake();

        Ford ford = new Ford(6, "Ford Falcon");
        ford.startEngine();
        ford.accelerate();
        ford.brake();

        Holden holden = new Holden(6, "Holden Commodore");
        holden.startEngine();
        holden.accelerate();
        holden.brake();
    }

    static class Car
    {
        private boolean engine;
        private int cylinders, wheels;
        private String name;

        public Car(int cylinders, String name)
        {
            this.cylinders = cylinders;
            this.name = name;

            this.wheels = 4;
            this.engine = true;
        }

        public void startEngine()
        {
            System.out.println("Car's engine is starting...");
        }

        public void accelerate()
        {
            System.out.println("Accelerating...");
        }

        public void brake()
        {
            System.out.println("Braking...");
        }

        public int getCylinders()
        {
            return cylinders;
        }

        public String getName()
        {
            return name;
        }
    }

    static class Mitsubishi extends Car
    {
        public Mitsubishi(int cylinders, String name)
        {
            super(cylinders, name);
        }

        @Override
        public void startEngine()
        {
            System.out.println("Mitsubishi engine is starting...");
        }

        @Override
        public void accelerate()
        {
            System.out.println("Mitsubishi is accelerating...");
        }

        @Override
        public void brake()
        {
            System.out.println("Mitsubishi is braking...");
        }
    }

    static class Holden extends Car
    {
        public Holden(int cylinders, String name)
        {
            super(cylinders, name);
        }

        @Override
        public void startEngine()
        {
            System.out.println("Holden engine is starting...");
        }

        @Override
        public void accelerate()
        {
            System.out.println("Holden is accelerating...");
        }

        @Override
        public void brake()
        {
            System.out.println("Holden is braking...");
        }
    }

    static class Ford extends Car
    {
        public Ford(int cylinders, String name)
        {
            super(cylinders, name);
        }

        @Override
        public void startEngine()
        {
            System.out.println("Ford's engine is starting...");
        }

        @Override
        public void accelerate()
        {
            System.out.println("Ford is accelerating...");
        }

        @Override
        public void brake()
        {
            System.out.println("Ford is braking...");
        }
    }
}
