package pl.globallogic.exercises.intermediate;

public class Ex13NumberInWord
{
    /*

    The method needs to print "ZERO", "ONE", "TWO", ... "NINE", "OTHER"
    if the int parameter number is 0, 1, 2, .... 9 or other for any other number
    including negative numbers.

     */
    public static void main(String[] args)
    {
        printNumberInWord(0);
        printNumberInWord(4);
        printNumberInWord(9);
        printNumberInWord(-2);
    }

    private static void printNumberInWord(int number)
    {
        System.out.println(NumberInWord(number));
    }

    private static String NumberInWord(int number)
    {
        if(number < 0 || number > 9) return "OTHER";

        String answer = "";

        switch (number)
        {
            case 0:
                    answer = "ZERO";
                break;
                case 1:
                    answer = "ONE";
                break;
                case 2:
                    answer = "TWO";
                break;
                case 3:
                    answer = "THREE";
                break;
                case 4:
                    answer = "FOUR";
                break;
                case 5:
                    answer = "FIVE";
                break;
                case 6:
                    answer = "SIX";
                break;
                case 7:
                    answer = "SEVEN";
                break;
                case 8:
                    answer = "EIGHT";
                break;
                case 9:
                    answer = "NINE";
                break;
        }
        return answer;
    }
}
