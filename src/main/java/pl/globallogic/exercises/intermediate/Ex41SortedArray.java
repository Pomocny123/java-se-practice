package pl.globallogic.exercises.intermediate;

import java.util.Arrays;
import java.util.Scanner;

public class Ex41SortedArray
{
    /*

    Create a program using arrays that sorts a list of integers in descending order.
    Descending order is highest value to lowest.

     */
    public static void main(String[] args)
    {
        int length = 6;
        int[] array = new int[length];

        array = getIntegers(length);

        printArray(array);

        array = sortIntegers(array);

        System.out.println(Arrays.toString(array));
    }

    public static int[] getIntegers(int sizeOfArray)
    {
        int iterations = 0;
        Scanner input = new Scanner(System.in);
        int[] array = new int[sizeOfArray];
        System.out.printf("Provide %d numbers to array:\n", sizeOfArray);

        while (iterations < sizeOfArray)
        {
            array[iterations] = input.nextInt();
            iterations++;
        }

        return array;
    }

    public static void printArray(int[] array)
    {
        int currentElement = 0;

        while (currentElement < array.length)
        {
            System.out.printf("Element %d contents %d\n", currentElement, array[currentElement]);
            currentElement++;
        }
    }

    public static int[] sortIntegers(int[] array)
    {
        int temp = 0;

        for (int i = 0; i < array.length; i++)
        {
            for (int j = 1; j < array.length; j++)
            {
                if(array[j-1] < array[j])
                {
                    temp = array[j-1];
                    array[j-1] = array[j];
                    array[j] = temp;
                }
            }
        }

        return array;
    }
}
