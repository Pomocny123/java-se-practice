package pl.globallogic.exercises.intermediate;

public class Ex38Encapsulation
{
    /*

    Create one class and name it Printer. This class will have two
    member variables of type int, tonerLevel and pagesPrinted, and
    one of type boolean called duplex. All three fields will have
    private access. The constructor will accept two of these
    variables, tonerLevel and duplex, as parameters following these rules:

    tonerLevel must be greater than -1 but less than or equal to 100.
    If it does not meet these conditions then it should be
    initialized to -1.

    duplex should be initialized to the value of the parameter.

     */
    public static void main(String[] args)
    {
        Printer printer = new Printer(50, true);
        System.out.println(printer.addToner(50));
        System.out.println("initial page count = " +printer.getPagesPrinted());
        int pagesPrinted = printer.printPages(4);
        System.out.println("Pages printed was " + pagesPrinted +" new total print count for printer = " +printer.getPagesPrinted());
        pagesPrinted = printer.printPages(2);
        System.out.println("Pages printed was " + pagesPrinted +" new total print count for printer = " +printer.getPagesPrinted());
    }

    static class Printer
    {
        private int tonerLevel, pagesPrinted;
        private boolean duplex;

        public Printer(int tonerLevel, boolean duplex)
        {
            if(tonerLevel > -1 && tonerLevel <= 100)
                this.tonerLevel = tonerLevel;
            else
                this.tonerLevel = -1;

            this.duplex = duplex;
        }

        public int addToner(int tonerAmount)
        {
            if(tonerAmount <= 0 || tonerAmount > 100) return -1;

            if(tonerLevel + tonerAmount > 100) return -1;

            tonerLevel += tonerAmount;

            return tonerLevel;
        }

        public int printPages(int pages)
        {
            int pagesToPrint = pages;

            if(duplex)
                pagesToPrint /= 2;

            pagesPrinted += pagesToPrint;

            return pagesToPrint;
        }

        public int getPagesPrinted()
        {
            return pagesPrinted;
        }
    }
}
