package pl.globallogic.exercises.intermediate;

public class Ex28PaintJob
{
    /*

    Bob is a wall painter and he needs your help. You have to write a program
    that helps Bob calculate how many buckets of paint he needs to buy before
    going to work. Bob might also have some extra buckets at home. He also
    knows the area that he can cover with one bucket of paint.

     */
    public static void main(String[] args)
    {
        System.out.println(getBucketCount(-3.4, 2.1, 1.5, 2));
        System.out.println(getBucketCount(3.4,2.1,1.5,2));
        System.out.println(getBucketCount(2.75,3.25,2.5,1));

        System.out.println(getBucketCount(-3.4, 2.1, 1.5));
        System.out.println(getBucketCount(3.4,2.1,1.5));
        System.out.println(getBucketCount(7.25,4.3,2.35));

        System.out.println(getBucketCount(3.4, 1.5));
        System.out.println(getBucketCount(6.26,2.2));
        System.out.println(getBucketCount(3.26,0.75));
    }

    private static int getBucketCount(double width, double height, double areaPerBucket, int extraBucket)
    {
        if(width <= 0 || height <= 0 || areaPerBucket <= 0 || extraBucket < 0)
            return -1;

        double areaToPaint = height * width;

        if (areaToPaint <= (extraBucket * areaPerBucket))
            return 0;

        int bucketsToBuy = 0;

        while (((bucketsToBuy * areaPerBucket) + (extraBucket * areaPerBucket)) < areaToPaint)
        {
            bucketsToBuy++;
        }

        return bucketsToBuy;
    }

    private static int getBucketCount(double width, double height, double areaPerBucket)
    {
        if(width <= 0 || height <= 0 || areaPerBucket <= 0)
            return -1;

        double areaToPaint = height * width;

        int bucketsToBuy = 1;

        while ((bucketsToBuy * areaPerBucket) < areaToPaint)
        {
            bucketsToBuy++;
        }

        return bucketsToBuy;
    }

    private static int getBucketCount(double area, double areaPerBucket)
    {
        if(area <=0 || areaPerBucket <= 0)
            return -1;

        int bucketsToBuy = 1;

        while (area > (bucketsToBuy * areaPerBucket))
        {
            bucketsToBuy++;
        }

        return bucketsToBuy;
    }
}
