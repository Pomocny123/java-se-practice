package pl.globallogic.exercises.intermediate;

public class Ex19SharedDigit
{
    /*

    Write a method named hasSharedDigit with two parameters of type int.

    Each number should be within the range of 10 (inclusive) - 99
    (inclusive). If one of the numbers is not within the range, the
    method should return false.

    The method should return true if there is a digit that appears in
    both numbers, such as 2 in 12 and 23; otherwise, the method should
    return false.

     */
    public static void main(String[] args)
    {
        System.out.println(hasSharedDigit(12,23));
        System.out.println(hasSharedDigit(9,99));
        System.out.println(hasSharedDigit(15,55));
        System.out.println(hasSharedDigit(26,79));
    }

    private static boolean hasSharedDigit(int firstNumber, int secondNumber)
    {
        if(firstNumber < 10 || firstNumber > 99 || secondNumber < 10 || secondNumber > 99)
            return false;

        int[] digitsOfBothNumbers = {(firstNumber / 10),
                (firstNumber % 10),
                (secondNumber / 10),
                (secondNumber % 10),};

        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if(i != j)
                    if(digitsOfBothNumbers[i] == digitsOfBothNumbers[j])
                        return true;
            }
        }

        return false;
    }
}
