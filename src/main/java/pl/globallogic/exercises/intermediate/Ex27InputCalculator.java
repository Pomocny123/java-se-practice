package pl.globallogic.exercises.intermediate;

import java.util.Arrays;
import java.util.Scanner;

public class Ex27InputCalculator
{
    /*

    Write a method called inputThenPrintSumAndAverage that does not
    have any parameters.

    The method should not return anything (void) and it needs to keep
    reading int numbers from the keyboard.

    When the user enters something that is not an int then it needs
    to print a message in the format "SUM = XX AVG = YY".

    XX represents the sum of all entered numbers of type int.
    YY represents the calculated average of all numbers of type long.

     */
    public static void main(String[] args)
    {
        inputThenPrintSumAndAverage();
    }

    private static void inputThenPrintSumAndAverage()
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter numbers:");
        String inputNumber;
        int[] inputAllNumbers = new int[32];
        int iteration = 0;

        while (true)
        {
            try
            {
                inputNumber = input.nextLine();
                inputAllNumbers[iteration] = Integer.parseInt(inputNumber);
                iteration++;
            }
            catch (NumberFormatException e)
            {
                break;
            }
        }

        int sum = 0;
        int avg = 0;
        iteration = 0;

        for (int i = 0; i < inputAllNumbers.length; i++)
        {
            sum += inputAllNumbers[i];
            if(inputAllNumbers[i] != 0) iteration++;
        }

        avg = sum / iteration;

        System.out.println(String.format("SUM = %s AVG = %s", sum, avg));
    }
}
