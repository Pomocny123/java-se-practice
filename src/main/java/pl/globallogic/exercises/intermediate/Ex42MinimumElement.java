package pl.globallogic.exercises.intermediate;

import java.util.Scanner;

public class Ex42MinimumElement
{
    /*

    Write a method called readInteger() that has no parameters and returns an int.
    It needs to read in an integer from the user - this represents how many elements the user needs to enter.
    Write another method called readElements() that has one parameter of type int
    The method needs to read from the console until all the elements are entered, and then return an array containing the elements entered.

     */

    public static void main(String[] args)
    {
        int numberOfElements = readInteger();
        int[] array = new int[numberOfElements];

        array = readElements(numberOfElements);

        System.out.printf("Minimum in array: %d", findMin(array));
    }

    public static int readInteger()
    {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter number of elements that should be in an array:\n");
        int numberOfElements = input.nextInt();

        return numberOfElements;
    }

    private static int[] readElements(int numberOfElements)
    {
        int[] array = new int[numberOfElements];
        int currentElement = 0;
        Scanner input = new Scanner(System.in);

        System.out.printf("Enter %d elements to array:\n", numberOfElements);

        while (currentElement < numberOfElements)
        {
            array[currentElement] = input.nextInt();

            currentElement++;
        }

        return array;
    }

    public static int findMin(int[] array)
    {
        int min = array[0];

        for (int i = 1; i < array.length; i++)
        {
            if(array[i] < min)
                min = array[i];
        }

        return min;
    }
}
