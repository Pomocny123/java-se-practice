package pl.globallogic.exercises.intermediate;

public class Ex25FlourPackProblem
{
    /*

    Write a method named canPack with three parameters of type
    int named bigCount, smallCount, and goal.

    The parameter bigCount represents the count of big flour bags
     (5 kilos each).

    The parameter smallCount represents the count of small flour
     bags (1 kilo each).

    The parameter goal represents the goal amount of kilos of
    flour needed to assemble a package.

     */
    public static void main(String[] args)
    {
        System.out.println(canPack(1,0,4));
        System.out.println(canPack(1,0,5));
        System.out.println(canPack(0,5,4));
        System.out.println(canPack(2,2,11));
        System.out.println(canPack(-3,2,12));

        System.out.println(canPack(2,0,9));
        System.out.println(canPack(1,4,9));
    }

    private static boolean canPack(int bigCount, int smallCount, int goal)
    {
        if(bigCount < 0 || smallCount < 0 || goal < 0) return false;

        if((bigCount * 5) + smallCount < goal) return false;
        if((bigCount * 5) + smallCount == goal) return true;

        if((bigCount * 5) == goal) return true;

        if(smallCount >= goal) return true;

        for (int i = 1; i <= bigCount; i++)
        {
            for (int j = 1; j <= smallCount; j++)
            {
                if((i*5) + j == goal) return true;
            }
        }

        return false;
    }
}
