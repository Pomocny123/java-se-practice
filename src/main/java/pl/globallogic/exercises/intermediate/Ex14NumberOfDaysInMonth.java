package pl.globallogic.exercises.intermediate;

public class Ex14NumberOfDaysInMonth
{
    /*

    Write a method isLeapYear with a parameter of type int named year.
    The parameter needs to be greater than or equal to 1 and less than
    or equal to 9999.

    If the parameter is not in that range return false.
    Otherwise, if it is in the valid range, calculate if the year is
    a leap year and return true if it is, otherwise return false.

     */
    public static void main(String[] args)
    {
        System.out.println(getDaysInMonth(1,2020));
        System.out.println(getDaysInMonth(2,2020));
        System.out.println(getDaysInMonth(2,2018));
        System.out.println(getDaysInMonth(-1,2020));
    }
    private static boolean isLeapYear(int year)
    {
        if(year < 1 || year > 9999) return false;

        if(year % 4 == 0 && year % 100 != 0) return true;

        if(year % 400 == 0) return true;

        return false;
    }
    private static int getDaysInMonth(int month, int year)
    {
        if((month < 1 || month > 12) || (year < 1 || year > 9999))
            return -1;

        boolean isLeap = isLeapYear(year);
        int answer = 0;

        switch (month)
        {
            case 1,3,5,7,8,10,12:
                answer = 31;
                break;
            case 4,6,9,11:
                answer = 30;
                break;
            case 2:
                if(isLeap)
                    answer = 29;
                else
                    answer = 28;
                break;
        }

        return answer;
    }
}
