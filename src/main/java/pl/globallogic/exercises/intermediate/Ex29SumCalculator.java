package pl.globallogic.exercises.intermediate;

public class Ex29SumCalculator
{
    /*

    Write a class with the name SimpleCalculator. The class needs two
    fields (instance variables) with names firstNumber and secondNumber
    both of type double.


     */
    public static void main(String[] args)
    {
        SimpleCalculator calculator = new SimpleCalculator();
        calculator.setFirstNumber(5.0);
        calculator.setSecondNumber(4);
        System.out.println("add= " + calculator.getAdditionResult());
        System.out.println("subtract= " + calculator.getSubtractionResult());
        calculator.setFirstNumber(5.25);
        calculator.setSecondNumber(0);
        System.out.println("multiply= " + calculator.getMultiplicationResult());
        System.out.println("divide= " + calculator.getDivisionResult());
    }

    private static class SimpleCalculator
    {
        double firstNumber;
        double secondNumber;

        public void setFirstNumber(double firstNumber)
        {
            this.firstNumber = firstNumber;
        }

        public void setSecondNumber(double secondNumber)
        {
            this.secondNumber = secondNumber;
        }

        public double getFirstNumber()
        {
            return firstNumber;
        }

        public double getSecondNumber()
        {
            return secondNumber;
        }

        public double getAdditionResult()
        {
            double answer = 0;

            answer = firstNumber + secondNumber;

            return answer;
        }

        public double getSubtractionResult()
        {
            double answer = 0;

            answer = firstNumber - secondNumber;

            return answer;
        }

        public double getMultiplicationResult()
        {
            double answer = 0;

            answer = firstNumber * secondNumber;

            return answer;
        }

        public double getDivisionResult()
        {
            if(secondNumber == 0) return 0;

            double answer = 0;

            answer = firstNumber / secondNumber;

            return answer;
        }
    }
}
