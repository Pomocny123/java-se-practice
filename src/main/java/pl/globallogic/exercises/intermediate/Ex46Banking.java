package pl.globallogic.exercises.intermediate;

import java.util.ArrayList;

public class Ex46Banking
{
    /*

    Create a simple banking application.

     */

    public static void main(String[] args)
    {
        Bank bank = new Bank("National Australia Bank");

        bank.addBranch("Adelaide");

        bank.addCustomer("Adelaide", "Tim", 50.05);
        bank.addCustomer("Adelaide", "Mike", 175.34);
        bank.addCustomer("Adelaide", "Percy", 220.12);

        bank.addCustomerTransaction("Adelaide", "Tim", 44.22);
        bank.addCustomerTransaction("Adelaide", "Tim", 12.44);
        bank.addCustomerTransaction("Adelaide", "Mike", 1.65);

        bank.listCustomers("Adelaide", false);



    }

    static class Bank
    {
        String name;
        ArrayList<Branch> branches = new ArrayList<Branch>();

        public Bank(String name)
        {
            this.name = name;
        }

        public boolean addBranch(String nameOfTheBranch)
        {
            Branch newBranch = new Branch(nameOfTheBranch);

            return branches.add(newBranch);
        }

        public boolean addCustomer(String nameOfBranch, String nameOfCustomer, double initialTransaction)
        {
            for (int i = 0; i < branches.size(); i++)
            {
                Branch branch = branches.get(i);

                if(branch.getName().equals(nameOfBranch))
                {
                    Customer customer = new Customer(nameOfCustomer, initialTransaction);

                    return branch.customers.add(customer);
                }
            }

            return false;
        }

        public boolean addCustomerTransaction(String nameOfBranch, String nameOfCustomer, double transaction)
        {
            for (int i = 0; i < branches.size(); i++)
            {
                Branch branch = branches.get(i);

                if(branch.getName().equals(nameOfBranch))
                {
                    return branch.addCustomerTransaction(nameOfCustomer, transaction);
                }
            }

            return false;
        }

        public Branch findBranch(String nameOfBranch)
        {
            for (int i = 0; i < branches.size(); i++)
            {
                Branch branch = branches.get(i);

                if(branch.getName().equals(nameOfBranch))
                {
                    return branch;
                }
            }

            return null;
        }

        public boolean listCustomers(String nameOfBranch, boolean printTransactions)
        {
            for (int i = 0; i < branches.size(); i++)
            {
                Branch branch = branches.get(i);

                if(branch.getName().equals(nameOfBranch))
                {
                    if(printTransactions)
                    {
                        System.out.printf("Customer details for branch %s\n", branch.getName());

                        for (int j = 0; j < branch.customers.size(); j++)
                        {
                            Customer customer = branch.customers.get(j);
                            System.out.printf("Customer: %s[%d]\n", customer.getName(), j+1);
                            System.out.println("Transactions");
                            for (int k = 0; k < customer.transactions.size(); k++)
                            {
                                System.out.printf("[%d]  Amount %.2f\n", k+1, customer.transactions.get(k));
                            }
                        }
                    }
                    else
                    {
                        System.out.printf("Customer details for branch %s\n", branch.getName());

                        for (int j = 0; j < branch.customers.size(); j++)
                        {
                            Customer customer = branch.customers.get(j);
                            System.out.printf("Customer: %s[%d]\n", customer.getName(), j+1);
                        }
                    }

                    return true;
                }
            }

            return false;
        }
    }

    static class Branch
    {
        String name;
        ArrayList<Customer> customers = new ArrayList<Customer>();

        public Branch(String name)
        {
            this.name = name;
        }

        public String getName()
        {
            return name;
        }

        public ArrayList<Customer> getCustomers()
        {
            return customers;
        }

        public boolean newCustomer(String nameOfCustomer, double initialTransaction)
        {
            Customer newCustomer = new Customer(nameOfCustomer, initialTransaction);

            return customers.add(newCustomer);
        }

        public boolean addCustomerTransaction(String nameOfCustomer, double transaction)
        {
            Customer customer = null;

            for (int i = 0; i < customers.size(); i++)
            {
                customer = customers.get(i);
                if(customer.getName().equals(nameOfCustomer))
                {
                    customer.transactions.add(transaction);
                    return true;
                }
            }

            return false;
        }

        public Customer findCustomer(String nameOfCustomer)
        {
            for (int i = 0; i < customers.size(); i++)
            {
                Customer customer = customers.get(i);
                if(customer.getName().equals(nameOfCustomer))
                {
                    return customer;
                }
            }

            return null;
        }
    }

    static class Customer
    {
        String name;

        ArrayList<Double> transactions = new ArrayList<Double>();

        public Customer(String name, double transaction)
        {
            this.name = name;
            this.transactions.add(transaction);
        }

        public String getName()
        {
            return name;
        }

        public ArrayList<Double> getTransactions()
        {
            return transactions;
        }

        public void addTransaction(double transaction)
        {
            transactions.add(transaction);
        }
    }
}
