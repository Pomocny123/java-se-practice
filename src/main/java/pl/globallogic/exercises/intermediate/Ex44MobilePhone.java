package pl.globallogic.exercises.intermediate;

import java.util.ArrayList;

public class Ex44MobilePhone
{
    /*

    Create a program that implements a simple mobile phone with the following capabilities.

     */

    public static void main(String[] args)
    {
        ArrayList<Contact> myContacts = new ArrayList<Contact>();
        Contact friend1 = new Contact("Aleks", "234123567");
        Contact friend2 = new Contact("Marek", "223568647");

        myContacts.add(friend1);
        myContacts.add(friend2);

        MobilePhone phone1 = new MobilePhone("111222333", myContacts);

        phone1.printContacts();
    }

    static class MobilePhone
    {
        String myNumber;
        ArrayList<Contact> myContacts;

        public MobilePhone(String myNumber, ArrayList<Contact> myContacts)
        {
            this.myNumber = myNumber;
            this.myContacts = myContacts;
        }

        public boolean addNewContact(Contact contact)
        {
            if (findContact(contact.getName()) >= 0) return false;

            myContacts.add(contact);
            return true;
        }

        public boolean updateContact(Contact oldContact, Contact newContact)
        {
            int position = findContact(oldContact);

            if(position < 0) return false;

            myContacts.set(position, newContact);
            return true;
        }

        public boolean removeContact(Contact to_remove)
        {
            int position = findContact(to_remove);

            if(position < 0) return false;

            myContacts.remove(position);
            return true;
        }

        public int findContact(Contact contact)
        {
            return myContacts.indexOf(contact);
        }

        public int findContact(String name)
        {
            for (int i = 0; i < myContacts.size(); i++)
            {
                Contact contact = myContacts.get(i);
                if (contact.getName().equals(name)) return i;
            }

            return -1;
        }

        public Contact queryContact(String name_of_contact)
        {
            int position = findContact(name_of_contact);

            if(position < 0) return null;

            return myContacts.get(position);
        }

        public void printContacts()
        {
            System.out.println("Contact List:\n");

            for (int i = 0; i < myContacts.size(); i++)
            {
                System.out.printf("%d. %s -> %s\n", i+1, myContacts.get(i).getName(), myContacts.get(i).phoneNumber);
            }
        }
    }

    static class Contact
    {
        String name;
        String phoneNumber;

        public Contact(String name, String phoneNumber)
        {
            this.name = name;
            this.phoneNumber = phoneNumber;
        }

        public String getName()
        {
            return name;
        }

        public String getPhoneNumber()
        {
            return phoneNumber;
        }

        static public Contact createContact(String name, String phoneNumber)
        {
            return new Contact(name, phoneNumber);
        }
    }
}
