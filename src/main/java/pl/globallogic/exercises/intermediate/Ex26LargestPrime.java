package pl.globallogic.exercises.intermediate;

public class Ex26LargestPrime
{
    /*

    Write a method named getLargestPrime with one parameter of type
    int named number.

    If the number is negative or does not have any prime numbers,
    the method should return -1 to indicate an invalid value.

    The method should calculate the largest prime factor of a given
    number and return it.

     */
    public static void main(String[] args)
    {
        System.out.println(getLargestPrime(21));
        System.out.println(getLargestPrime(217));
        System.out.println(getLargestPrime(0));
        System.out.println(getLargestPrime(45));
        System.out.println(getLargestPrime(-2));
    }

    private static int getLargestPrime(int number)
    {
        if(number <= 0) return -1;

        int[] factors = new int[20];
        int factor = 2;
        int iteration = 0;

        while (number > 1)
        {
            while (number % factor == 0)
            {
                factors[iteration] = factor;
                number /= factor;
                iteration++;
            }
            factor++;
        }

        int max = 0;

        for (int i = 0; i < 20; i++)
        {
            if(factors[i] > max) max = factors[i];
        }

        return max;
    }
}
