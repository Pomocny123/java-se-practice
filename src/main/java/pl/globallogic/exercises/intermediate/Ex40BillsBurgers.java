package pl.globallogic.exercises.intermediate;

public class Ex40BillsBurgers
{
    /*

    The purpose of this application is to help a company called Bill's Burgers manage the process of selling
    their hamburgers. And in order to match Bill's menu, you will need to create three(3) classes, Hamburger,
    DeluxeBurger, and HealthyBurger.

     */
    public static void main(String[] args)
    {
        Hamburger hamburger = new Hamburger("Basic", "Sausage", 3.56, "White");
        hamburger.addHamburgerAddition1("Tomato", 0.27);
        hamburger.addHamburgerAddition2("Lettuce", 0.75);
        hamburger.addHamburgerAddition3("Cheese", 1.13);
        System.out.println("Total Burger price is " + hamburger.itemizeHamburger());

        HealthyBurger healthyBurger = new HealthyBurger("Bacon", 5.67);
        healthyBurger.addHamburgerAddition1("Egg", 5.43);
        healthyBurger.addHealthyAddition1("Lentils", 3.41);
        System.out.println("Total Healthy Burger price is  " + healthyBurger.itemizeHamburger());

        DeluxeBurger db = new DeluxeBurger();
        db.addHamburgerAddition3("Should not do this", 50.53);
        System.out.println("Total Deluxe Burger price is " + db.itemizeHamburger());
    }

    static class Hamburger
    {
        String name, meat, breadRollType;
        double price;

        String addition1Name, addition2Name, addition3Name, addition4Name;
        double addition1Price = 0, addition2Price = 0, addition3Price = 0, addition4Price = 0;

        public Hamburger(String name, String meat, double price, String breadRollType)
        {
            this.name = name;
            this.meat = meat;
            this.breadRollType = breadRollType;
            this.price = price;
            System.out.printf("Basic hamburger on a %s with %s, price is %.2f\n", breadRollType, meat, price);
        }

        public void addHamburgerAddition1(String name, double price)
        {
            this.addition1Name = name;
            if(price >= 0)
                this.addition1Price = price;
            else
                this.addition1Price = 0;
        }
        public void addHamburgerAddition2(String name, double price)
        {
            this.addition2Name = name;
            if(price >= 0)
                this.addition2Price = price;
            else
                this.addition2Price = 0;
        }
        public void addHamburgerAddition3(String name, double price)
        {
            this.addition3Name = name;
            if(price >= 0)
                this.addition3Price = price;
            else
                this.addition3Price = 0;
        }
        public void addHamburgerAddition4(String name, double price)
        {
            this.addition4Name = name;
            if(price >= 0)
                this.addition4Price = price;
            else
                this.addition4Price = 0;
        }

        public double itemizeHamburger()
        {
            double totalPrice = 0;

            if(addition1Price > 0.0)
                System.out.printf("Added %s for extra %.2f\n", addition1Name, addition1Price);

            if(addition2Price > 0.0)
                System.out.printf("Added %s for extra %.2f\n", addition2Name, addition2Price);

            if(addition3Price > 0.0)
                System.out.printf("Added %s for extra %.2f\n", addition3Name, addition3Price);

            if(addition4Price > 0.0)
                System.out.printf("Added %s for extra %.2f\n", addition4Name, addition4Price);

            totalPrice = price + addition1Price + addition2Price + addition3Price + addition4Price;

            return totalPrice;
        }
    }

    static class DeluxeBurger extends Hamburger
    {
        public DeluxeBurger()
        {
            super("Deluxe Burger", "Sausage & Bacon", 14.54, "White roll");
            System.out.printf("Deluxe hamburger on a %s with %s, price is %.2f\n", breadRollType, meat, 14.54);
        }

        @Override
        public void addHamburgerAddition1(String name, double price)
        {
            System.out.println("Can't add additions to delux burger.");
        }
        @Override
        public void addHamburgerAddition2(String name, double price)
        {
            System.out.println("Can't add additions to delux burger.");
        }
        @Override
        public void addHamburgerAddition3(String name, double price)
        {
            System.out.println("Can't add additions to delux burger.");
        }
        @Override
        public void addHamburgerAddition4(String name, double price)
        {
            System.out.println("Can't add additions to delux burger.");
        }

        @Override
        public double itemizeHamburger()
        {
            double totalPrice = price;
            double chips = 2.75, drink = 1.81;

            System.out.printf("Added Chips for an extra %.2f\n", chips);
            System.out.printf("Added Drink for an extra %.2f\n", drink);

            totalPrice += chips + drink;

            return totalPrice;
        }
    }

    static class HealthyBurger extends Hamburger
    {
        String healthyExtra1Name, healthyExtra2Name, healthyExtra3Name, healthyExtra4Name;
        double healthyExtra1Price, healthyExtra2Price, healthyExtra3Price, healthyExtra4Price;

        public HealthyBurger(String meat, double price)
        {
            super("Healthy Burger", meat, price, "Brown rye roll");
            System.out.printf("%s on a %s with %s, price is %.2f\n", name, breadRollType, meat, price);
        }

        public void addHealthyAddition1(String name, double price)
        {
            System.out.printf("Added %s for an extra %.2f\n", name, price);
        }

        public void addHealthyAddition2(String name, double price)
        {
            System.out.printf("Added %s for an extra %.2f\n", name, price);
        }
    }
}
