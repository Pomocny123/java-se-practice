package pl.globallogic.exercises.intermediate;

public class Ex32Point
{
    /*

    You have to represent a point in 2D space. Write a class with the name Point.
    The class needs two fields (instance variables) with name x and y of type int.
    The class needs to have two constructors. The first constructor does not have
    any parameters (no-arg constructor). The second constructor has parameters x
    and y of type int and it needs to initialize the fields.

     */
    public static void main(String[] args)
    {
        Point first = new Point(6, 5);
        Point second = new Point(3, 1);
        System.out.println("distance(0,0)= " + first.distance());
        System.out.println("distance(second)= " + first.distance(second));
        System.out.println("distance(2,2)= " + first.distance(2, 2));
        Point point = new Point();
        System.out.println("distance()= " + point.distance());
    }

    static class Point
    {
        int x,y;

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
        public Point()
        {

        }

        public int getX()
        {
            return x;
        }

        public void setX(int x)
        {
            this.x = x;
        }

        public int getY()
        {
            return y;
        }

        public void setY(int y)
        {
            this.y = y;
        }

        public double distance()
        {
            double distance = Math.sqrt(Math.pow((0 - x), 2) + Math.pow((0 - y), 2));

            return distance;
        }

        public double distance(int x, int y)
        {
            double distance = Math.sqrt(Math.pow((this.x - x), 2) + Math.pow((this.y - y), 2));;

            return distance;
        }

        public double distance(Point A)
        {
            double distance = Math.sqrt(Math.pow((this.x - A.x), 2) + Math.pow((this.y - A.y), 2));;

            return distance;
        }
    }
}
