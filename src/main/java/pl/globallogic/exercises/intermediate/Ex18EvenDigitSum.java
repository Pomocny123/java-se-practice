package pl.globallogic.exercises.intermediate;

public class Ex18EvenDigitSum
{
    /*

    Write a method named getEvenDigitSum with one parameter of type int called number.

    The method should return the sum of the even digits within the number.

    If the number is negative, the method should return -1 to indicate an invalid value.

     */
    public static void main(String[] args)
    {
        System.out.println(getEvenDigitSum(123456789));
        System.out.println(getEvenDigitSum(252));
        System.out.println(getEvenDigitSum(-22));
    }

    private static int getEvenDigitSum(int number)
    {
        if (number < 0) return -1;

        int copyNumber = number;
        int singleDigit = 0;
        int sum = 0;

        while (copyNumber > 0)
        {
            singleDigit = copyNumber % 10;
            if (singleDigit % 2 ==0) sum += singleDigit;

            copyNumber /= 10;
        }

        return sum;
    }
}
