package pl.globallogic.exercises.intermediate;

import java.util.ArrayList;
import java.util.List;

public class Ex48Interface
{
    /*

    Create a simple interface that allows an object to be saved to some sort of storage medium.
    Implement the following:
    1. ISaveable (interface)
    2. Player (class)
    3. Monster (class)

     */

    public static void main(String[] args)
    {
        Player player1 = new Player("Tim", 10, 15);
        Monster monster1 = new Monster("Werewolf", 20, 40);

        List<String> players = player1.write();
        List<String> monsters = monster1.write();

        player1.read(players);
        monster1.read(monsters);

        String resultPlayer, resultMonster;

        resultPlayer = player1.toString();
        resultMonster = monster1.toString();

        System.out.printf("%s\n%s", resultPlayer, resultMonster);
    }

    interface ISaveable
    {
        List<String> write();
        void read(List<String> list);
    }

    static class Player implements ISaveable
    {
        String name, weapon;
        int hitPoints, strength;

        public Player(String name, int hitPoints, int strength)
        {
            this.name = name;
            this.hitPoints = hitPoints;
            this.strength = strength;
            this.weapon = "Sword";
        }

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public String getWeapon()
        {
            return weapon;
        }

        public void setWeapon(String weapon)
        {
            this.weapon = weapon;
        }

        public int getHitPoints()
        {
            return hitPoints;
        }

        public void setHitPoints(int hitPoints)
        {
            this.hitPoints = hitPoints;
        }

        public int getStrength()
        {
            return strength;
        }

        public void setStrength(int strength)
        {
            this.strength = strength;
        }

        @Override
        public List<String> write()
        {
            List<String> list = new ArrayList<>();
            list.add(0, this.name);
            list.add(1, "" + this.hitPoints);
            list.add(2, "" + this.strength);
            list.add(3, this.weapon);

            return list;
        }

        @Override
        public void read(List<String> savedList)
        {
            if(savedList != null && !savedList.isEmpty())
            {
                this.name = savedList.get(0);
                this.hitPoints = Integer.parseInt(savedList.get(1));
                this.strength = Integer.parseInt(savedList.get(2));
                this.weapon = savedList.get(3);
            }
        }

        @Override
        public String toString()
        {
            return "Player{name='"+ this.name +"', hitpoints="+ this.hitPoints +", " +
                    "strength="+ this.strength +",\nweapon='"+ this.weapon +"'}\n";
        }
    }

    static class Monster implements ISaveable
    {
        String name;
        int hitPoints, strength;

        public Monster(String name, int hitPoints, int strength)
        {
            this.name = name;
            this.hitPoints = hitPoints;
            this.strength = strength;
        }

        public String getName()
        {
            return name;
        }

        public int getHitPoints()
        {
            return hitPoints;
        }

        public int getStrength()
        {
            return strength;
        }

        @Override
        public List<String> write()
        {
            List<String> list = new ArrayList<>();
            list.add(0, this.name);
            list.add(1, "" + this.hitPoints);
            list.add(2, "" + this.strength);

            return list;
        }

        @Override
        public void read(List<String> savedList)
        {
            if(savedList != null && !savedList.isEmpty())
            {
                this.name = savedList.get(0);
                this.hitPoints = Integer.parseInt(savedList.get(1));
                this.strength = Integer.parseInt(savedList.get(2));
            }
        }

        @Override
        public String toString()
        {
            return "Monster{name='"+ this.name +"', hitpoints="+ this.hitPoints +", " +
                    "strength="+ this.strength +"}\n";
        }
    }
}
