package pl.globallogic.exercises.intermediate;

public class Ex16NumberPalindrome
{
    /*

    Write a method called isPalindrome with one int parameter called number.

    The method needs to return a boolean.

    It should return true if the number is a palindrome number otherwise it
    should return false.

     */
    public static void main(String[] args)
    {
        System.out.println(isPalindrome(-1221));
        System.out.println(isPalindrome(707));
        System.out.println(isPalindrome(11212));
    }

    private static boolean isPalindrome(int number)
    {
        int remain = 0;
        int inveredNumber = 0;
        if(number < 0) number *= -1;
        int copyNumber = number;

        while (copyNumber > 0)
        {
            remain = copyNumber % 10;
            inveredNumber = (inveredNumber* 10) + remain;
            copyNumber /= 10;
        }

        return inveredNumber == number;
    }
}
