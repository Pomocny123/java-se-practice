package pl.globallogic.exercises.basic;

public class GenericSandbox
{
    public static void main(String[] args)
    {
        Box<String> smallBox = new Box<>("Jane");
        smallBox.setPayload("Jane");
        String name = smallBox.getPayload();

    }
}
