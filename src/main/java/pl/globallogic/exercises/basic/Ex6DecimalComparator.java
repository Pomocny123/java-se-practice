package pl.globallogic.exercises.basic;

public class Ex6DecimalComparator
{
    /*

    Write a method areEqualByThreeDecimalPlaces with two parameters of type double.
    The method should return boolean and it needs to return true if two double numbers
    are the same up to three decimal places. Otherwise, return false.

     */
    public static void main(String[] args)
    {
        System.out.println(areEqualByThreeDecimalPlaces(-3.1756, -3.175));
        System.out.println(areEqualByThreeDecimalPlaces(3.175, 3.176));
        System.out.println(areEqualByThreeDecimalPlaces(3.0, 3.0));
        System.out.println(areEqualByThreeDecimalPlaces(-3.123, 3.123));
    }

    private static boolean areEqualByThreeDecimalPlaces(double first, double second)
    {
        int firstAdjusted = (int)(first*1000.0);
        int secondAdjusted = (int)(second*1000.0);

        return firstAdjusted == secondAdjusted;
    }
}
