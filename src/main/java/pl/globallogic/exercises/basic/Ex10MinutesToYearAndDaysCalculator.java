package pl.globallogic.exercises.basic;

public class Ex10MinutesToYearAndDaysCalculator
{
    /*

    Write a method printYearsAndDays with parameter of type long named minutes.
    The method should not return anything (void) and it needs to calculate the years and
    days from the minutes parameter.
    If the parameter is less than 0, print text "Invalid Value".
    Otherwise, if the parameter is valid then it needs to print a message in the format
    "XX min = YY y and ZZ d".

     */
    public static void main(String[] args)
    {
        printYearsAndDays(525600);
        printYearsAndDays(1051200);
        printYearsAndDays(561600);
    }

    private static void printYearsAndDays(long minutes)
    {
        if(minutes < 0)
        {
            System.out.println("Invalid value");
            return;
        }
        long[] YearsAndDays = YearsAndDays(minutes);
        System.out.println(minutes + " min = " + YearsAndDays[0] + " y and " + YearsAndDays[1] + " d");
    }
    private static long[] YearsAndDays(long minutes)
    {
        long[] YearsAndDays = new long[2];

        YearsAndDays[0] = minutes / 525600;// 525600 = 60 * 24 * 365 ->  hoursFormat*daysFormat*yearFormat
        YearsAndDays[1] = minutes % 525600;
        YearsAndDays[1] = YearsAndDays[1] / 1440; // 1440 = 60 * 24 -> hoursFormat*daysFormat

        return YearsAndDays;
    }
}
