package pl.globallogic.exercises.basic;

public class Ex5LeapYearCalculator
{
    /*

    The parameter needs to be greater than or equal to 1 and less than or equal to 9999.

    If the parameter is not in that range return false.

    Otherwise, if it is in the valid range, calculate if the year is a leap year and return
    true if it is a leap year, otherwise return false.

     */
    public static void main(String[] args)
    {
        System.out.println(isLeapYear(-1600));
        System.out.println(isLeapYear(1600));
        System.out.println(isLeapYear(2017));
        System.out.println(isLeapYear(2000));
    }

    private static boolean isLeapYear(int year)
    {
        if(year < 1 || year > 9999) return false;

        if(year % 4 == 0 && year % 100 != 0) return true;

        if(year % 400 == 0) return true;

        return false;
    }
}
