package pl.globallogic.exercises.basic;

public class Palindrome
{
    public static void main(String[] args)
    {
        System.out.println(isPalindrome("abcd"));
        System.out.println(isPalindrome("ababa"));
        System.out.println(isPalindrome("abccab"));
        System.out.println(isPalindrome("abccba"));
    }

    private static boolean isPalindrome(String word)
    {
        String reverse = "";

        for (int i = word.length()-1; i >= 0; i--)
        {
            reverse += word.charAt(i);
        }

        return word.equals(reverse);
    }
}
