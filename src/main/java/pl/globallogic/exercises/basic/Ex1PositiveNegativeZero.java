package pl.globallogic.exercises.basic;

public class Ex1PositiveNegativeZero
{
    /*

    The method should not return any value, and it needs to print out:
    "positive" if the parameter number is > 0
    "negative" if the parameter number is < 0
    "zero" if the parameter number is equal to 0


     */
    public static void main(String[] args)
    {
        checkNumber(1);
        checkNumber(0);
        checkNumber(-1);
    }

    private static void checkNumber(int number)
    {
        if(number > 0)
        {
            System.out.println("Positive");
        }
        else if(number == 0)
        {
            System.out.println("Zero");
        }
        else
        {
            System.out.println("Negative");
        }
    }
}
