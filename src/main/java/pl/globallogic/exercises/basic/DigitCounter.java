package pl.globallogic.exercises.basic;

import java.util.Scanner;

public class DigitCounter
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number to count its digits:");
        int number  = scanner.nextInt();
        System.out.println("Digit count for " + number  + " is equal to " + numberOfDigits(number));

//        System.out.println(numberOfDigits(13345) == 5);
//        System.out.println(numberOfDigits(13345111) == 8);
//        System.out.println(numberOfDigits(-345) == -1);
    }

    private static int numberOfDigits(int targetNumber)
    {
        if(targetNumber < 0)
        {
            return -1;
        }

        int numberDigets = 0;
        while (targetNumber > 0)
        {
            targetNumber /= 10;
            numberDigets++;
        }

        return numberDigets;
    }
}
