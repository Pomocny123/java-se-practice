package pl.globallogic.exercises.basic;

public class Ex2SpeedConverter
{
    /*

    Program will converting kilometers to miles.

    */
    public static void main(String[] args)
    {
        printConversion(1.5);
        printConversion(10.25);
        printConversion(-5.6);
    }

    private static long toMilesPerHour(double kilometersPerHour)
    {
        if(kilometersPerHour >= 0)
        {
            return Math.round(kilometersPerHour / 1.609);
        }
        else
        {
            return -1;
        }
    }

    private static void printConversion(double kilometersPerHour)
    {
        long miles = toMilesPerHour(kilometersPerHour);
        if(miles < 0)
        {
            System.out.println("Invalid value");
            return;
        }
        System.out.println(kilometersPerHour + "km/h = " + miles + "ml/h");

    }
}
