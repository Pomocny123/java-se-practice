package pl.globallogic.exercises.basic;

import java.util.Arrays;

import static java.util.Arrays.sort;

public class CandyShop
{
    /*

    Max number of candy the boss can buy with his funds
    prices: [5,2,1,3,4]

     */
    public static void main(String[] args)
    {
        int[] candyPrices = {3,4,1,2,7,5,6,1};
        int money = 30;

        int[] sortedCandyPrices = sortCandies(candyPrices);
        System.out.printf("Sorted candy prices: %s", Arrays.toString(sortedCandyPrices));
        System.out.printf("Number of candies which can be bougth for %s$: %s",
                money,
                findNumberOfCandles(candyPrices, money));
    }

    private static int findNumberOfCandles(int[] candyPrices, int money)
    {
        return 0;
    }

    private static int[] sortCandies(int[] candyPrices)
    {
        int[] sortedCandyPrices = {};



        return sortedCandyPrices;
    }
}
