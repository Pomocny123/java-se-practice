package pl.globallogic.exercises.basic;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class NongenericCOllectionSandbox
{
    public static void main(String[] args)
    {
        List<String>nonGenericNameStorage = new ArrayList<>();
        nonGenericNameStorage.add("Joe");
        nonGenericNameStorage.add("Jane");
        nonGenericNameStorage.add("Jack");
//        nonGenericNameStorage.add(3);
//        nonGenericNameStorage.add(new StringBuilder("Cos tam"));

        for (String name : nonGenericNameStorage)
        {
            System.out.println(name.toUpperCase());
        }
    }
}
