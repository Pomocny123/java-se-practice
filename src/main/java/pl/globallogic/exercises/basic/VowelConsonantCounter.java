package pl.globallogic.exercises.basic;

public class VowelConsonantCounter
{
    /*

    Count vowels and consonants in the string
    vowels - a,i,o,u,e,y; consonants - other

     */
    public static void main(String[] args)
    {
        String word = "Words";
        int[] ans = printNumberOfVowelsAndConsonants(word);
        System.out.println(String.format("Number of vowels is %s and consonants is %s in '%s'",
                ans[0], ans[0], word));
    }
    private static int[] printNumberOfVowelsAndConsonants(String word)
    {
        String vowels = "aioueyAIOUEY";
        int vowelsCount = 0;
        int consonantsCount = 0;
        for (int i = 0; i < word.length(); i++)
        {
            if(vowels.contains(String.valueOf(word.charAt(i))));
            {
                vowelsCount++;
            }
        }
        consonantsCount = word.length() - vowelsCount;

        return new int[] {vowelsCount, consonantsCount};
    }
}
