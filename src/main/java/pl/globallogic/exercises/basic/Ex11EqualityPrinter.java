package pl.globallogic.exercises.basic;

public class Ex11EqualityPrinter
{
    /*

    Write a method printEqual with 3 parameters of type int. The method should not return
    anything (void).
    If one of the parameters is less than 0, print text "Invalid Value".
    If all numbers are equal print text "All numbers are equal"
    If all numbers are different print text "All numbers are different".
    Otherwise, print "Neither all are equal or different".

     */
    public static void main(String[] args)
    {
        printEqual(1,1,1);
        printEqual(1,1,2);
        printEqual(-1,-1,-1);
        printEqual(1,2,3);
    }

    private static void printEqual(int first, int second, int third)
    {
        if(first < 0 || second < 0 || third < 0)
        {
            System.out.println("Invalid value");
            return;
        }

        if(first == second && second == third)
        {
            System.out.println("All numbers are equal");
            return;
        }

        if(first != second && second != third && first != third)
        {
            System.out.println("All numbers are different");
            return;
        }

        System.out.println("Neither all are equal or different");
    }
}
