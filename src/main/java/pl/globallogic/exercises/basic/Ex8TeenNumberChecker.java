package pl.globallogic.exercises.basic;

public class Ex8TeenNumberChecker
{
    /*

    We'll say that a number is "teen" if it is in the range 13 -19 (inclusive).
    Write a method named hasTeen with 3 parameters of type int.
    The method should return boolean and it needs to return true if one of the parameters
    is in range 13(inclusive) - 19 (inclusive). Otherwise return false.


     */
    public static void main(String[] args)
    {
        System.out.println(hasTeen(9, 99, 19));
        System.out.println(hasTeen(23,15,42));
        System.out.println(hasTeen(22,23,34));
    }

    private static boolean hasTeen(int first, int second, int third)
    {
        boolean validFirst = true;
        boolean validSecond = true;
        boolean validThird = true;

        if(first < 13 || first > 19) validFirst = false;
        if(second < 13 || second > 19) validSecond = false;
        if(third < 13 || third > 19) validThird = false;

        return (validFirst || validSecond || validThird);
    }
}
