package pl.globallogic.exercises.basic;

public class Ex3MegaBytesConverter
{
    /*

    The method should not return anything (void) and it needs to calculate
    the megabytes and remaining kilobytes from the kilobytes parameter.


     */
    public static void main(String[] args)
    {
        printMegaBytesAndKiloBytes(2500);
        printMegaBytesAndKiloBytes(-1024);
        printMegaBytesAndKiloBytes(5000);
    }

    private static void printMegaBytesAndKiloBytes(int kiloBytes)
    {
        if (kiloBytes < 0)
        {
            System.out.println("Invalid value");
            return;
        }

        int megaBytes = kiloBytes / 1000;
        int remainingKiloBytes = kiloBytes % 1000;

        System.out.println(kiloBytes + "KB = " + megaBytes + " MB and " + remainingKiloBytes + " KB");
    }
}
