package pl.globallogic.exercises.basic;

import java.util.Scanner;

public class PrimeNumbersFinder
{
    public static void main(String[] args)
    {
        //Scanner input = new Scanner(System.in);
        int primeNumbersThreshold = 17;
        System.out.printf("Prime numbers before '%s': \n", primeNumbersThreshold);
        printPrimesFor(primeNumbersThreshold);
    }

    private static void printPrimesFor(int primeNumbersThreshold)
    {
        for (int i = 2; i < primeNumbersThreshold; i++)
        {
            if(isPrime(i))
                System.out.print(i + " ");
        }
    }

    private static boolean isPrime(int candidate)
    {
        for (int i = 2; i <= candidate / 2; i++)
        {
            if (candidate % i == 0)
                return false;
        }

        return true;
    }
}
