package pl.globallogic.bankingapp.model;

public class Account
{
    private final int id;
    private final String accountNumber;
    private double balance;

    public Account(int id, String accountNumber, double balance)
    {
        this.accountNumber = accountNumber;
        this.id = id;
        this.balance = balance;
    }

    public String getAccountNumber()
    {
        return accountNumber;
    }

    public int getId()
    {
        return id;
    }

    public double getBalance()
    {
        return balance;
    }

    public void setBalance(double balance)
    {
        this.balance = balance;
    }
}
