package pl.globallogic.bankingapp;

import pl.globallogic.bankingapp.dao.AccountDao;
import pl.globallogic.bankingapp.service.AccountService;
import pl.globallogic.bankingapp.service.BankingService;
import pl.globallogic.bankingapp.model.Account;

public class BankingAppRunner
{
    public static void main(String[] args)
    {
        final AccountDao accountDao = new AccountDao();
        final AccountService accountService = new AccountService(accountDao);
        final AccountService accountService2 = new AccountService(accountDao);
        final BankingService service = new BankingService(accountService);
        final BankingService service2 = new BankingService(accountService2);
        int accountId = 1;
        // withdraw test - happy path
        service.withdraw(accountId,100.00);
        final Account acc1 = accountService.getAccountById(accountId);
        double expectedBalanceAfterWithdraw = 180.00;
        System.out.println(acc1.getBalance() == expectedBalanceAfterWithdraw);
        // save account test - happy path
        final Account acc2 = accountService.getAccountById(1);
        System.out.println(acc2.getBalance() == expectedBalanceAfterWithdraw);
        // withdraw test - edge case (amount > balance)
        double amount = 200.00;
        service2.withdraw(accountId, amount);
        final Account acc3 = accountService.getAccountById(1);
        System.out.println(acc3.getBalance() == expectedBalanceAfterWithdraw);
        System.out.println(accountService.accountSavingCounter);
    }

}
